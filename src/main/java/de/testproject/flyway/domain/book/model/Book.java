package de.testproject.flyway.domain.book.model;

import java.util.UUID;

public record Book(
        UUID id,
        String title,
        String author,
        int fromYear
) {
}
