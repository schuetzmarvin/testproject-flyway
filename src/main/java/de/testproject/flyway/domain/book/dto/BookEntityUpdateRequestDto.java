package de.testproject.flyway.domain.book.dto;

public class BookEntityUpdateRequestDto {
    private int fromYear;

    public int getFromYear() {
        return this.fromYear;
    }
}
